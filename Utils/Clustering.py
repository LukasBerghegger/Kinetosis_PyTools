import numpy as np
import pandas
from kneed import KneeLocator
from sklearn.cluster import HDBSCAN
from sklearn.cluster import KMeans
from sklearn.cluster import MeanShift
from sklearn.cluster import estimate_bandwidth
from sklearn.metrics import silhouette_score


def elbow(s: pandas.DataFrame, weight: list[float], max_k: int) -> [int, list[int]]:
    sse = []
    labels = []

    for k in range(1, max_k + 1):
        print("evaluating k=" + str(k))

        km = KMeans(k)
        km = km.fit(s[["x", "y"]], sample_weight=weight)

        sse.append(km.inertia_)
        labels.append(km.labels_)

    kl = KneeLocator(
        range(1, len(sse) + 1), sse, curve="convex", direction="decreasing"
    )

    opt_k = kl.elbow

    if opt_k is None:
        opt_k = 1

    return opt_k, labels[opt_k - 1]


def silhouette(s: pandas.DataFrame, weight: list[float], max_k: int) -> [int, list[int]]:
    silhouette_coefficients = []
    labels = []
    k_range = range(2, max_k + 1)

    for k in k_range:
        print("evaluating k=" + str(k))

        km = KMeans(k)
        km = km.fit(s[["x", "y"]], sample_weight=weight)

        score = silhouette_score(s, labels=km.labels_)
        silhouette_coefficients.append(score)
        labels.append(km.labels_)

    opt_k = silhouette_coefficients.index(
        max(silhouette_coefficients, key=lambda x: abs(x))
    ) + 2

    if opt_k is None:
        opt_k = 1

    return opt_k, labels[opt_k - 2]


def kMeans_Clustering(s: pandas.DataFrame, max_k: int) -> [int, list[int]]:
    weight = []

    for e in s.index:
        if e + 1 < len(s):  # get gaze duration
            ms = s["timestamp"][e + 1] - s["timestamp"][e]
        else:  # catch end of array
            ms = s["timestamp"][e] - s["timestamp"][e - 1]

        weight.append(ms)

    opt_k, labels = silhouette(s, weight, max_k)

    print("\nopt_k=" + str(opt_k) + "\n")

    return opt_k, labels


def mean_shift_clustering(s: pandas.DataFrame) -> [int, list[int]]:
    bandwidth = estimate_bandwidth(s, quantile=0.2)

    ms = MeanShift(bandwidth=bandwidth, bin_seeding=True)  # todo find freq
    ms.fit(s[["x", "y"]])

    k = max(ms.labels_) + 1

    print("\nk=" + str(k) + "\n")

    return len(ms.cluster_centers_), ms.labels_  # size estimation is crude


def HDBSCAN_clustering(s: pandas.DataFrame) -> [int, list[int]]:
    hdb = HDBSCAN(allow_single_cluster=True, n_jobs=-1, cluster_selection_epsilon=0.02)  # todo find freq
    hdb.fit_predict(s[["x", "y"]])

    unique_labels = np.unique(hdb.labels_)
    k = len(unique_labels)

    print("\nk=" + str(k) + "\n")

    return k, hdb.labels_  # size estimation is crude
